# server/todo_app/tests/test_graphql_mutations.py

from graphql_jwt.shortcuts import get_token
from django.contrib.auth import get_user_model
from graphene_django.utils.testing import GraphQLTestCase
from todo_app.models import Todo, Project, Category
import json

class MutationTestCases(GraphQLTestCase):

  def setUp(self):
    self.user = get_user_model().objects.create(username='ron')
    self.token   = get_token(self.user)
    self.headers = {"HTTP_AUTHORIZATION": f"JWT {self.token}"}

  def test_create_todo(self):
    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    response = self.query(
      '''
      mutation createTodo($title: String!, $task: String!, $projId: Int!, $catId: Int!) {
        createTodo(title: $title, task: $task, projId: $projId, catId: $catId) {
          title
          task
          slug
          project {
            name
            slug
          }
          category {
            name
            slug
          }
        }
      }
      ''',
      variables = {
        'title': 'todo',
        'task': 'task',
        'projId': proj1.id,
        'catId':  cat1.id,
      },
      headers=self.headers,
    )

    expected = {
      'data': {
        'createTodo': {
          'title': 'todo',
          'task': 'task',
          'slug': 'todo',
          'project': {
            'name': 'proj1',
            'slug': 'proj1',
          },
          'category': {
            'name': 'cat1',
            'slug': 'cat1',
          }
        }
      }
    }

    content = json.loads(response.content)
    assert content ==  expected
    self.assertResponseNoErrors(response)

  def test_update_todo_name(self):
    # Test for updating a todos name

    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    todo = Todo(title='todo', task='todo task', project=proj1, category=cat1, posted_by=self.user)
    todo.save()

    response = self.query(
      '''
      mutation updateTodo($id: Int!, $title: String!) {
        updateTodo(id: $id, title: $title){
          id
          slug
          title
          task
          postedBy {
            id
            username
            email
          }
          project {
            slug
            name
          }
          category {
            slug
            name
            parent {
              slug
              name
            }
          }
        }
      }

      ''',
        variables = {
          'id': todo.id,
          'title': 'todo updated',
        },
        headers = self.headers,
      )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_todo_task(self):
    # Test for updating a todos task

    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    todo = Todo(title='todo', task='todo task', project=proj1, category=cat1, posted_by=self.user)
    todo.save()

    response = self.query(
        '''
        mutation updateTodo($id: Int!, $task: String!) {
          updateTodo(id: $id, task: $task){
            id
            slug
            title
            task
            project {
              slug
              name
            }
            category {
              slug
              name
              parent {
                slug
                name
              }
            }
          }
        }

        ''',
          variables = {
            'id': todo.id,
            'task': 'todo updated task',
          },
          headers=self.headers,
        )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_todo_project(self):
    # Test for updating a todos project

    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    todo = Todo(title='todo', task='todo task', project=proj1, category=cat1, posted_by=self.user)
    todo.save()

    proj2 = Project(name='proj2')
    proj2.save()

    response = self.query(
        '''
        mutation updateTodo($id: Int!, $projId: Int!) {
          updateTodo(id: $id, projId: $projId){
            id
            slug
            title
            task
            project {
              slug
              name
            }
            category {
              slug
              name
              parent {
                slug
                name
              }
            }
          }
        }

        ''',
          variables = {
            'id': todo.id,
            'projId': proj2.id,
          },
          headers=self.headers,
        )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_todo_category(self):
    # Test for updating a todos category

    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    todo = Todo(title='todo', task='todo task', project=proj1, category=cat1, posted_by=self.user)
    todo.save()

    cat2 = Category(name='cat2')
    cat2.save()

    response = self.query(
        '''
        mutation updateTodo($id: Int!, $catId: Int!) {
          updateTodo(id: $id, catId: $catId){
            id
            slug
            title
            task
            project {
              slug
              name
            }
            category {
              slug
              name
              parent {
                slug
                name
              }
            }
          }
        }

        ''',
          variables = {
            'id': todo.id,
            'catId': cat2.id,
          },
          headers=self.headers,
        )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_todo_is_completed(self):
    # Test for updating a todos is_completed status

    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    todo = Todo(title='todo', task='todo task', project=proj1, category=cat1, posted_by=self.user)
    todo.save()


    response = self.query(
        '''
        mutation updateTodo($id: Int!, $isCompleted: Boolean!) {
          updateTodo(id: $id, isCompleted: $isCompleted){
            id
            slug
            title
            task
            isCompleted
            project {
              slug
              name
            }
            category {
              slug
              name
              parent {
                slug
                name
              }
            }
          }
        }

        ''',
          variables = {
            'id': todo.id,
            'isCompleted': True,
          },
          headers=self.headers,
        )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_todo_everything(self):
    # Test for updating all of a todo's attributes at once

    proj1 = Project(name='proj1')
    proj1.save()

    cat1 = Category(name='cat1')
    cat1.save()

    proj2 = Project(name='proj2')
    proj2.save()

    cat2 = Category(name='cat2')
    cat2.save()

    todo = Todo(title='todo', task='todo task', project=proj1, category=cat1, posted_by=self.user)
    todo.save()


    response = self.query(
        '''
        mutation updateTodo(
          $id: Int!, 
          $title: String!, 
          $task: String!, 
          $projId: Int!, 
          $catId: Int!, 
          $isCompleted: Boolean!
          ) {
          updateTodo(
            id: $id, 
            title: $title, 
            task: $task, 
            projId: $projId, 
            catId: $catId, 
            isCompleted: $isCompleted,
            ){
            id
            slug
            title
            task
            isCompleted
            project {
              slug
              name
            }
            category {
              slug
              name
              parent {
                slug
                name
              }
            }
          }
        }

        ''',
          variables = {
            'id': todo.id,
            'title': 'updated title',
            'task': 'updated task',
            'projId': proj2.id,
            'catId': cat2.id,
            'isCompleted': True,
          },
          headers=self.headers,
        )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_create_project(self):
    response = self.query(
      '''
      mutation createProject($name: String!, $description: String!) {
        createProject(name: $name, description: $description) {
          id
          name
          description
          slug
        }
      }
      ''',
      variables = {
        'name': 'My Project',
        'description': 'Description for my project',
      },
      headers = self.headers
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_project_name(self):
    project = Project.objects.create(name='foo', posted_by=self.user)
    # Test updating name of a project

    response = self.query(
      '''
      mutation updateProject($id: Int!, $name: String!) {
        updateProject(id:$id, name: $name) {
          id
          name
          description
          slug
        }
      }
      ''',
      variables = {
        'id': project.id,
        'name': 'My Project',
      },
      headers = self.headers
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_project_description(self):
    # Test updating of project description
    project = Project.objects.create(name='foo', description='bar', posted_by=self.user)

    response = self.query(
      '''
      mutation updateProject($id: Int!, $description: String!) {
        updateProject(id:$id, description: $description) {
          id
          name
          description
          slug
        }
      }
      ''',
      variables = {
        'id': project.id,
        'description': 'Description for my project',
      },
      headers = self.headers
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_project_name_description(self):
    project = Project.objects.create(name='foo', posted_by=self.user)
    # Test updating both name and description of a project

    response = self.query(
      '''
      mutation updateProject($id: Int!, $name: String!, $description: String!) {
        updateProject(id:$id, name: $name, description: $description) {
          id
          name
          description
          slug
        }
      }
      ''',
      variables = {
        'id': project.id,
        'name': 'My Project',
        'description': 'Description for my project',
      },
      headers = self.headers
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_delete_project(self):
    project = Project.objects.create(name='foo', posted_by=self.user)

    response = self.query(
      '''
      mutation deleteProject($id: Int!) {
        deleteProject(id: $id) {
          id
          name
        }
      }
      ''',
      variables = {
        'id': project.id,
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_create_category_no_parent(self):
    response = self.query(
      '''
      mutation createCategory($name: String!) {
        createCategory(name: $name) {
          id
          name
          slug
        }
      }
      ''',
      variables = {
        'name': 'my category',
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_create_category_with_parent(self):
    parent = Category.objects.create(name='Category Parent', posted_by=self.user)

    response = self.query(
      '''
      mutation createCategory($name: String!, $parentId: Int!) {
        createCategory(name: $name, parentId: $parentId) {
          id
          name
          slug
        }
      }
      ''',
      variables = {
        'name': 'my category',
        'parentId': parent.id
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
  
  def test_update_category_name(self):
    category = Category.objects.create(name='foo', posted_by=self.user)
    # Test for changing a category's name

    response = self.query(
      '''
      mutation updateCategory($id: Int!,$name: String!){
        updateCategory(id: $id, name: $name) {
          id
          name
          slug
        }
      }
      ''',
      variables = {
        'id': category.id,
        'name': 'New Category Name',
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_category_parent(self):
    categoryParent = Category.objects.create(name='Parent Category', posted_by=self.user)
    category = Category.objects.create(name='foo', posted_by=self.user)
    # Test for moving category into a parent category.

    response = self.query(
      '''
      mutation updateCategory($id: Int!,$parentId: Int!){
        updateCategory(id: $id, parentId: $parentId) {
          id
          name
          slug
          parent {
            id
            name
            slug
          }
        }
      }
      ''',
      variables = {
        'id': category.id,
        'parentId': categoryParent.id,
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_update_category_name_parent(self):
    categoryParent = Category.objects.create(name='category parent', posted_by=self.user)
    category = Category.objects.create(name='foobar', posted_by=self.user)
    # Test for changing a category's name & moving renamed category into a parent category
    
    response = self.query(
      '''
      mutation updateCategory($id: Int!, $name: String!, $parentId: Int!){
        updateCategory(id: $id, name: $name, parentId: $parentId) {
          id
          name
          slug
          parent {
            id
            name
            slug
          }
        }
      }
      ''',
      variables = {
        'id': category.id,
        'name': 'New Category Name',
        'parentId': categoryParent.id
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)

  def test_delete_category(self):
    category = Category.objects.create(name='foo', posted_by=self.user)

    response = self.query(
      '''
      mutation deleteCategory($id: Int!) {
        deleteCategory(id: $id) {
          id
          name
        }
      }
      ''',
      variables = {
        'id': category.id,
      },
      headers = self.headers,
    )

    content = json.loads(response.content)
    self.assertResponseNoErrors(response)