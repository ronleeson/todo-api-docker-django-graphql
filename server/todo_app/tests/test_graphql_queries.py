# server/todo_app/tests/test_graphql_queries.py
 
from graphene_django.utils.testing import GraphQLTestCase
from todo_app.models import Todo, Project, Category
import json
 
class QueryTestCases(GraphQLTestCase):
  def test_all_todos(self):
    response = self.query(
      '''
      query GetTodos {
        todos {
          id
          title
          task
          slug
          project {
            id
            name
            slug
          }
          category {
            id
            name
            slug
          }
           
        }
      }
      '''
    )
 
    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
 
  def test_todo_by_id_query(self):
    proj1 = Project.objects.create(name='proj1')
    cat1 = Category.objects.create(name='cat1')
    todo1 = Todo.objects.create(id=1, title='todo1', task='todo1 task', project=proj1, category=cat1)
     
    response = self.query(
      '''
        query Todo($id: Int!) {
          todo(id:$id) {
            id
            title
            task
            slug
            project {
              name
              slug
            }
            category {
              name
              slug
            }
          }
        }
      ''',
      variables = {
        'id': 1
      }
    )
 
    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
 
  def test_all_projects(self):
    proj1 = Project.objects.create(name='proj1')
    cat1 = Category.objects.create(name='cat1')
    todo1 = Todo.objects.create(title='todo1', task='todo1 task', project=proj1, category=cat1)
 
    response = self.query(
      '''
        query GetProjects {
            projects {
              id
              name
              slug
              todoSet {
                id
                title
                task
                slug
                category {
                  id
                  name
                  slug
                }
              }
            }
          }
      '''
    )
 
    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
 
  def test_project_by_id_query(self):
    proj1 = Project.objects.create(id=1, name='proj1')
    cat1 = Category.objects.create(name='cat1')
    todo1 = Todo.objects.create(title='todo1', task='todo1 task', project=proj1, category=cat1)
 
    response = self.query(
      '''
        query Project($id: Int!) {
          project(id:$id) {
            id
            name
            slug
            todoSet {
              id
              title
              task
              slug
              category {
                id
                name
                slug
              }
            }
          }
        }
      ''',
      variables = {
        'id': 1
      }
    )
 
    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
 
  def test_all_categories_query(self):
 
    response = self.query(
      '''
        query GetCategories {
          categories {
            id
            name
            todos {
              id
              title
              project {
                id
                name
              }
            }
          }
        }
      '''
    )
 
    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
 
  def test_get_category_by_id(self):
    cat1 = Category.objects.create(id=1, name='cat1')
    proj1 = Project.objects.create(id=1, name='proj1')
    todo = Todo.objects.create(id=1, title='todo', task='todo task', project=proj1)
 
    response = self.query(
      '''
        query GetCategory($id: Int!) {
          category(id: $id) {
            id
            name
            slug
            todos {
              id
              title
              task
              slug
              project {
                id
                name
                slug
              }
            }
          }
        }
      ''',
      variables = {
        'id': 1
      }
    )
 
    content = json.loads(response.content)
    self.assertResponseNoErrors(response)
