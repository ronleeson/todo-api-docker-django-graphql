# /server/todo_app/tests.py
  
from django.test import TestCase
  
from todo_app.models import Category, Todo, Project
  
class CategoryModelTest(TestCase):
  def test_string_representation(self):
    category = Category(name="my category name")
    self.assertEqual(str(category), category.name)
  
class TodoModelTest(TestCase):
  def test_string_representation(self):
    todo = Todo(title="my todo title")
    self.assertEqual(str(todo), todo.title)
  
  def test_todo_field(self):
    # create category object
    cat = Category(name="Frontend")
 
    # create project object
    proj = Project(name="My Project")
  
    todo = Todo(
      title = "my todo title",
      task = "foo",
      category = cat,
      project = proj,
      is_completed = False,
      due_date = "2021-07-30",
    )
    self.assertEqual(todo.title, "my todo title")
    self.assertEqual(todo.task, "foo")
    self.assertEqual(todo.category.name, "Frontend")
    self.assertEqual(todo.project.name, "My Project")
    self.assertEqual(todo.is_completed, False)
    self.assertEqual(todo.due_date, "2021-07-30")
  
class ProjectModelTest(TestCase):
  def test_string_representation(self):
    project = Project(name = "my project name")
    self.assertEqual(str(project), project.name)
  
  def test_project_field(self):
    proj = Project(
      name = "my project name",
      description = "my project description",
    )
    self.assertEqual(proj.name, "my project name")
    self.assertEqual(proj.description, "my project description")