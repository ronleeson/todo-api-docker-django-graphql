# server/todo_app/models.py
 
from django.db import models
from autoslug import AutoSlugField
from django.utils import timezone
from datetime import datetime
from django.conf import settings
from mptt.models import MPTTModel, TreeForeignKey
 
class Category(MPTTModel):
  name = models.CharField(max_length=50, unique=True)
  parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
  posted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
  slug = AutoSlugField(populate_from='name')
  modified = models.DateTimeField(auto_now=True, null=True, editable=False) 
   
  created = models.DateField(default=timezone.now().strftime("%Y-%m-%d")) 
 
  def __str__(self): 
    return self.name 
 
  class MPTTMeta: 
    order_insertion_by = ['name'] 
    unique_together = ('slug', 'parent') 
 
  class Meta: 
    verbose_name_plural = 'categories'
 
class Project(models.Model):
  name = models.CharField(max_length=500)
  description = models.TextField(max_length=2500, blank=True, null=True, help_text="Description of your project.")
  posted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
  slug = AutoSlugField(populate_from='name')
  modified = models.DateTimeField(auto_now=True, null=True, editable=False) 
  created = models.DateField(default=timezone.now().strftime("%Y-%m-%d")) 
 
  def __str__(self): 
    return self.name 
 
  class Meta: 
    verbose_name_plural = 'projects'
 
     
class Todo(models.Model):
  title = models.CharField(max_length=5000)
  task = models.TextField(blank=True)
  slug = AutoSlugField(populate_from='title')
  category = TreeForeignKey('Category', on_delete=models.CASCADE, null=True, blank=True, related_name='todos')
 
  project = models.ForeignKey('Project', on_delete=models.CASCADE, null=True, blank=True)
  posted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
  is_completed = models.BooleanField(default=False)
 
  modified = models.DateTimeField(auto_now=True, null=True, editable=False)
  created = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
  due_date = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
 
  def __str__(self):
    return self.title