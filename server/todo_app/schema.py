# /server/todo_app/schema.py
 
import graphene
from graphene import Schema
from graphene_django import DjangoObjectType
from graphql import GraphQLError
from todo_app.models import Todo, Project, Category
from users.schema import UserType
 
class TodoType(DjangoObjectType):
  class Meta:
    model = Todo
 
class ProjectType(DjangoObjectType):
  class Meta:
    model = Project
 
class CategoryType(DjangoObjectType):
  class Meta:
    model = Category
 
class Query(graphene.ObjectType):
   
  # todo queries
  todos = graphene.List(TodoType)
  def resolve_todos(self, info, **kwargs):
    return Todo.objects.all()
 
  todo = graphene.Field(
    TodoType,
    id=graphene.Int(),
  ) 
  def resolve_todo(self, info, id):
    return Todo.objects.get(pk=id)
 
  # project queries
  projects = graphene.List(ProjectType)
  def resolve_projects(self, info, **kwargs):
    return Project.objects.all()
 
  project = graphene.Field(
    ProjectType,
    id=graphene.Int(),
  )
  def resolve_project(self, info, id):
    return Project.objects.get(pk=id)
 
  # category queries
  categories = graphene.List(CategoryType)
  def resolve_categories(self, info, **kwargs):
    return Category.objects.all()
 
  category = graphene.Field(
    CategoryType,
    id=graphene.Int(),
  )
  def resolve_category(self, info, id):
    return Category.objects.get(pk=id)
 
# Mutations
class CreateTodo(graphene.Mutation):
  id = graphene.Int()
  title = graphene.String()
  task = graphene.String()
  is_completed = graphene.Boolean()
  slug = graphene.String()
  project = graphene.Field(ProjectType)
  category = graphene.Field(CategoryType)
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    title = graphene.String()
    task = graphene.String()
    projId = graphene.Int()
    catId = graphene.Int()
 
  def mutate(self, info, title, task, projId=None, catId=None):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    if projId:
      proj = Project.objects.get(pk=projId)
    else:
      proj = None
 
    if catId:
      cat = Category.objects.get(pk=catId)
    else:
      cat = None
       
    todo = Todo.objects.create(
      title=title, 
      task=task, 
      project=proj, 
      category=cat,
      posted_by=user,
    )
 
    return CreateTodo(
      id=todo.id,
      title=todo.title,
      task=todo.task,
      slug=todo.slug,
      project=todo.project,
      category=todo.category,
      posted_by=todo.posted_by,
    )
 
class UpdateTodo(graphene.Mutation):
  id = graphene.Int()
  title = graphene.String()
  task = graphene.String()
  slug = graphene.String()
  is_completed = graphene.Boolean()
  project = graphene.Field(ProjectType)
  category = graphene.Field(CategoryType)
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    id = graphene.Int()
    title = graphene.String()
    task = graphene.String()
    is_completed = graphene.Boolean()
    projId = graphene.Int()
    catId = graphene.Int()
 
  def mutate(self, info, id, title=None, task=None, is_completed=None, projId=None, catId=None):
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    todo = Todo.objects.get(pk=id)
 
    # test if logged in user is the same as posted_by
    if todo.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if projId:
      project = Project.objects.get(pk=projId)
      if not project:
        raise Exception('There is no project with this id.')
 
    if catId:
      category = Category.objects.get(pk=catId)
      if not category:
        raise Exception('No category with this id exists.')
 
    if todo:
      if title:
        todo.title = title
      if task:
        todo.task = task
      if is_completed:
        todo.is_completed = is_completed
      if projId:
        todo.project = project
      if catId:
        todo.category = category
 
      todo.save()
 
      return UpdateTodo(
        id = todo.id,
        title = todo.title,
        task = todo.task,
        slug = todo.slug,
        is_completed = todo.is_completed,
        project = todo.project,
        category = todo.category,
        posted_by=todo.posted_by,
      )
    else:
      raise Exception('There is no todo with this id.')
 
class DeleteTodo(graphene.Mutation):
  id = graphene.Int()
  title = graphene.String()
  ok = graphene.Boolean()
 
  class Arguments:
    id = graphene.Int()
 
  def mutate(self, info, id=id):
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    todo = Todo.objects.get(pk=id)
     
    # test if logged in user is the same as posted_by
    if todo.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if todo:
      todo.delete()
      return DeleteTodo(
        id = todo.id,
        title = todo.title,
        ok = True
      )
 
class UpdateTodo(graphene.Mutation):
  id = graphene.Int()
  title = graphene.String()
  task = graphene.String()
  is_completed = graphene.Boolean()
  project = graphene.Field(ProjectType)
  category = graphene.Field(CategoryType)
  slug = graphene.String()
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    id = graphene.Int()
    title = graphene.String()
    task = graphene.String()
    is_completed = graphene.Boolean()
    projId = graphene.Int()
    catId = graphene.Int()
 
  def mutate(self, info, id, title=None, task=None, is_completed=None, projId=None, catId=None):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    todo = Todo.objects.get(pk=id)
 
    # test if logged in user is the same as posted_by
    if todo.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if projId:
      project = Project.objects.get(pk=projId)
      if not project:
        raise Exception('There is no project with this id.')
 
    if catId:
      category = Category.objects.get(pk=catId)
      if not category:
        raise Exception('No category with this id exists.')
 
    if todo:
      if title:
        todo.title = title
      if task:
        todo.task = task
      if is_completed:
        todo.is_completed = is_completed
      if projId:
        todo.project = project
      if catId:
        todo.category = category
 
      todo.save()
 
      return UpdateTodo(
        id = todo.id,
        title = todo.title,
        task = todo.task,
        is_completed = todo.is_completed,
        project = todo.project,
        category = todo.category,
        slug = todo.slug,
        posted_by=todo.posted_by,
      )
    else:
      raise Exception('There is no todo with this id.')
 
class CreateProject(graphene.Mutation):
  id = graphene.Int()
  name = graphene.String()
  description = graphene.String()
  slug = graphene.String()
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    name = graphene.String()
    description = graphene.String()
 
  def mutate(self, info, name, description=None):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    project = Project.objects.create(
      name=name, 
      description=description,
      posted_by=user,
    )
 
    return CreateProject(
      id = project.id,
      name = project.name,
      description = project.description,
      slug = project.slug,
      posted_by = project.posted_by,
    )
 
class DeleteProject(graphene.Mutation):
  id = graphene.Int()
  name = graphene.String()
  ok = graphene.Boolean()
 
  class Arguments:
    id = graphene.Int()
 
  def mutate(self, info, id=id):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
     
    project = Project.objects.get(pk=id)
 
    # test if logged in user is the same as posted_by
    if project.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if project:
      project.delete()
      return DeleteProject(
        id = project.id,
        name = project.name,
        ok = True,
      )
 
class UpdateProject(graphene.Mutation):
  id = graphene.Int()
  name = graphene.String()
  description = graphene.String()
  slug = graphene.String()
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    id = graphene.Int()
    name = graphene.String()
    description = graphene.String()
   
  def mutate(self, info, id=id, name=None, description=None):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    project = Project.objects.get(pk=id)
 
    # test if logged in user is the same as posted_by
    if project.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if project:
      if name:
        project.name = name
      if description:
        project.description = description
 
      project.save()
 
      return UpdateProject(
        id = project.id,
        name = project.name,
        description = project.description,
        slug = project.slug, 
        posted_by = project.posted_by,
      )
    else:
      raise Exception('There is no project with this id.')
 
class CreateCategory(graphene.Mutation):
  id = graphene.Int()
  name = graphene.String()
  slug = graphene.String()
  parent = graphene.Field(CategoryType)
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    name = graphene.String()
    parentId = graphene.Int()
 
  def mutate(self, info, name, parentId=None):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    if parentId:
      parent = Category.objects.get(pk=parentId)
      category = Category.objects.create(
        name=name, 
        parent=parent,
        posted_by=user,
      )
    else:
      category = Category.objects.create(
        name=name,
        posted_by=user,
      )
 
    return CreateCategory(
      id=category.id,
      name=category.name,
      slug=category.slug,
      parent=category.parent,
      posted_by=category.posted_by,
    )
 
class DeleteCategory(graphene.Mutation):
  id = graphene.Int()
  name = graphene.String()
  slug = graphene.String()
  ok = graphene.Boolean()
 
  class Arguments:
    id = graphene.Int()
 
  def mutate(self, info, id=id):
 
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
 
    category = Category.objects.get(pk=id)
 
    # test if logged in user is the same as posted_by
    if category.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if category:
      category.delete()
      return DeleteCategory(
        id = category.id,
        name = category.name,
        slug = category.slug,
        ok = True
      )
 
class UpdateCategory(graphene.Mutation):
  id = graphene.Int()
  name = graphene.String()
  parent = graphene.Field(CategoryType)
  slug = graphene.String()
  posted_by = graphene.Field(UserType)
 
  class Arguments:
    id = graphene.Int()
    name = graphene.String()
    parentId = graphene.Int()
 
  def mutate(self, info, id, name=None, parentId=None):
     
    user = info.context.user
    if user.is_anonymous:
      raise GraphQLError('You must be logged in to execute this mutation!')
     
    category = Category.objects.get(pk=id)
 
    # test if logged in user is the same as posted_by
    if category.posted_by.id != user.id:
      raise GraphQLError('You do not have permission to execute this mutation!')
 
    if parentId:
      parent = Category.objects.get(pk=parentId)
      if not parent:
        raise Exception('No category with this id to be used as the updated parent.')
 
 
    if category:
      if name:
        category.name = name
      if parentId:
        category.parent = parent
       
      category.save()
 
      return UpdateCategory(
        id = category.id,
        name = category.name,
        parent = category.parent,
        slug = category.slug,
        posted_by = category.posted_by,
      )
    else:
      raise Exception('No category with this id.')      
 
 
 
class Mutation(graphene.ObjectType):
  create_todo = CreateTodo.Field()
  update_todo = UpdateTodo.Field()
  delete_todo = DeleteTodo.Field()
  create_project = CreateProject.Field()
  delete_project = DeleteProject.Field()
  update_project = UpdateProject.Field()
  create_category = CreateCategory.Field()
  delete_category = DeleteCategory.Field()
  update_category = UpdateCategory.Field()
 
schema = Schema(query=Query, mutation=Mutation)