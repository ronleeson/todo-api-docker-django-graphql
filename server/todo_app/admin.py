# todo-app-graphql/server/todo_app/admin.py
 
from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from mptt.admin import DraggableMPTTAdmin
from mptt.admin import TreeRelatedFieldListFilter
 
from todo_app.models import( 
        Category, 
        Todo,
        Project,
    )
 
admin.site.register(
    Category, 
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
    ),
    list_display_links=(
        'indented_title',
    )
)
 
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'created', 'modified')
 
admin.site.register(Project, ProjectAdmin)
 
class TodoAdmin(admin.ModelAdmin):
    model = Todo 
    list_filter = (
        ('category', TreeRelatedFieldListFilter),
    )
    list_display = ('title', 'is_completed', 'created', 'modified')
 
admin.site.register(Todo, TodoAdmin)