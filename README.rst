===================================================
Todo List API Built with Docker, Django and GraphQL
===================================================

An API serving graphql endpoints for a Django Todo Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--------------------------------------------------------------

This is a basic Django Todo app serving GraphQl endpoints.

Requirements:
~~~~~~~~~~~~~

You will need the following installed on your host machine:

- Docker
- Docker Compose
- GNU make (optional)

------------------

DB schema:
~~~~~~~~~~

- Project
 - Category
  - Todo Task
  
----------------	
    
**Example query**::

	{
  		project(id: 1) {
    		id
    		name
    		slug
    		todoSet {
      			id
      			title
      			task
      			slug
      			category {
        			id
        			name
        			slug
      			}
    		}
  	}
	


  
  
**Example endpoint**::

	{
  		"data": {
    	"project": {
      		"id": "1",
      		"name": "Project 1",
      		"slug": "project-1",
      		"todoSet": [
        		{
          			"id": "1",
          			"title": "todo1 updated",
          			"task": "Do it",
          			"slug": "my-first-todo",
          			"category": {
            		"id": "1",
            		"name": "Category 1",
            		"slug": "category-1"
          	}
        },
        {
          "id": "7",
          "title": "todo1 updated",
          "task": "Do this task…",
          "slug": "my-todo",
          "category": {
            "id": "1",
            "name": "Category 1",
            "slug": "category-1"
          }
        }
      ]
    }
  }
}
  
======================================
    
Booting Up
~~~~~~~~~~


To boot up the app run::

	# Build server
  docker-compose build server

  # Spin up database
  docker-compose up -d database

  # Spin up server
  docker-compose up -d server

  # Migrate user app DB
  docker-compose run server ./manage.py makemigrations
  docker-compose run server ./manage.py Migrate

  # Create super user
  docker-compose run server ./manage.py createsuperuser
    
Once all the initial commands above have been run you can use the simple make command to spin up the app::

	make prod
    
Django Admin
~~~~~~~~~~~~

To access Django admin UI::

	http://localhost:5555/admin

To setup a user::

  docker-compose run server ./manage.py createsuperuser
    
GraphiQL Playground
~~~~~~~~~~~~~~~~~~

To access the GrapiQL playground::

	http://localhost:5555/graphql
    
    
Testing
~~~~~~~

To run pytest::

	docker-compose run server pytest
    
or using the make command::

	make pytest
    
See the Makefile for all the other available make commands.

This app includes:

- JWT User Authentication
- Django-Graphene
- MPTT (Modified Preorder Tree Traversal)
- GraphiQL Playground
- Tests convering GraphQL Queries and Mutations
- PostgreSQL for the database